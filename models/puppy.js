const mongoose = require('mongoose')

const PuppySchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  name: {
    type: String,
    unique: true,
    required: true,
    default: 'Lexie'
  },
  breed: {
    type: String,
    required: false,
    default: 'Cavachon'
  },
  age: {
    type: Number,
    required: false,
    default: 1,
    min: [0, 'Age must be 0 (puppy) or more'],
    max: [25, 'Age must be 25 or less'],
    validate: {
      validator: Number.isInteger,
      message: 'Age must be an integer. {VALUE} is not an integer'
    }
  },
  parents: {
    type: Array,
    required: false,
    default: [
      {
        parentName: 'Mom',
        parentBreed: 'Bichon Frise',
        parentAge: 5
      },
      {
        parentName: 'Dad',
        parentBreed: 'Cavalier King Charles',
        parentAge: 6
      }
    ]
  }
})

module.exports = mongoose.model('Puppy', PuppySchema)
