const mongoose = require('mongoose')

const TransactionSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  transactionnum: { type: Number, required: true },
  transactiontype: { type: String, required: true },
  transactionamount: { type: Number, required: true },
  transactiondate: { type: Date, required: true },
})

module.exports = mongoose.model('Transaction', TransactionSchema)
