const mongoose = require('mongoose')

const CategorySchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  name: {
    type: String,
    unique: true,
    required: true,
    default: '001'
  },
  detail: {
    type: String,
    required: false,
    default: 'Savings'
  },
})

module.exports = mongoose.model('Category', CategorySchema)