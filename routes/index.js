/**
 * @index.js - manages all routing
 *
 * router.get when assigning to a single request
 * router.use when deferring to a controller
 *
 * @requires express
 */

const express = require('express')
const LOG = require('../utils/logger.js')

LOG.debug('START routing')
const router = express.Router()

// Manage top-level request first
router.get('/', (req, res, next) => {
  LOG.debug('Request to /')
  res.render('index.ejs', { title: 'Puppies!' })
})

// Add top-level close request
router.get('/close', (req, res) => {
  LOG.info('Closing down...')
  LOG.info('Received close request, shutting down.')
  process.exit()
})

// Defer path requests to a particular controller
router.use('/about', require('../controllers/about.js'))
router.use('/puppy', require('../controllers/puppy.js'))
router.use('/users', require('../controllers/users.js'))
router.use('/transactions', require('../controllers/transaction.js'))

LOG.debug('END routing')
module.exports = router
