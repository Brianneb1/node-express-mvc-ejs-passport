process.env.NODE_ENV = 'test'
const app = require('../../app.js')
var User = require('../../models/user.js')
const LOG = require('../../utils/logger.js')
const mocha = require('mocha')
const expect = require('chai').expect
var request = require('supertest')
const EMAIL = 'test@test.com'
const PASSWORD = 'Password_1'

LOG.debug('Starting test/controllers/user-spec.js.')

mocha.describe('API Tests - User Controller - Login', function () {
  mocha.describe('GET /login', function () {
    mocha.it('responds with status 200', function (done) {
      request(app)
        .get('/login')
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.status).to.be.equal(200)
          done()
        })
    })
  })
})

mocha.describe('API Tests - User Controller', function () {
  mocha.beforeEach(function (done) {
    var user = new User({
      username: EMAIL,
      password: PASSWORD
    })

    user.save(function (error) {
      if (error) {
        LOG.error('Error: ' + error.message)
      } else { LOG.info('no error') }
      done()
    })
  })

  mocha.it('find a user by email', function (done) {
    User.findOne({ email: EMAIL }, function (err, user) {
      if (err) { return done(err, 'Error finding user.') }
      if (user == null) { return done(err, 'No user found.') }
      expect(user.email).to.equal(EMAIL)
      LOG.info('   email: ', user.email)
      done()
    })
  })

  // Clear users after testing
  mocha.after(function () {
    return User.remove().exec()
  })

  mocha.describe('GET /signup', function () {
    var token

    mocha.before(function (done) {
      request(app)
        .post('/signup')
        .send({
          email: EMAIL,
          password: PASSWORD
        })
        .expect(302) // found
        .expect('Content-Type', 'text/plain; charset=utf-8')
        .end(function (err, res) {
          if (err) return done(err)
          token = res.body.token
          done()
        })
    })

    mocha.it('should respond with a user profile when authenticated', function (done) {
      request(app)
        .get('/account')
        .set('authorization', 'Bearer ' + token)
        .expect(302)
        .expect('Content-Type', 'text/plain; charset=utf-8')
        .end(function (err, res) {
          if (err) return done(err)
          done()
        })
    })

    mocha.it('should respond with a 401 when not authenticated', function (done) {
      request(app)
        .get('/logout')
        .expect(302)
        .end(done)
    })
  })
})
