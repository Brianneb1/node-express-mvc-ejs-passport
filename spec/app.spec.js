//jasmine provides function "describe"
//arg 1 is description
//arg 2 is function/logic

var request = require('request')

const base_url = 'https://localhost:8089'

describe('Test app.js', () => {

    describe('GET /', () =>{
        it('returns a status code of 200', () =>{
            request.get(base_url, (error, response, body) =>{
                expect(response.statusCode).toBe(200)
            })
        })
    })

    describe('GET /about', () =>{
        it('returns a status code of 200', () =>{
            request.get(base_url, (error, response, body) =>{
                expect(response.statusCode).toBe(200)
            })
        })
    })

    describe('GET /account', () =>{
        it('returns a status code of 200', () =>{
            request.get(base_url, (error, response, body) =>{
                expect(response.statusCode).toBe(200)
            })
        })
    })

    describe('GET /puppy', () =>{
        it('returns a status code of 200', () =>{
            request.get(base_url, (error, response, body) =>{
                expect(response.statusCode).toBe(200)
            })
        })
    })

    describe('GET /woo', () =>{
        it('returns a status code of 404', () =>{
            request.get(base_url, (error, response, body) =>{
                expect(response.statusCode).toBe(404)
            })
        })
    })
})