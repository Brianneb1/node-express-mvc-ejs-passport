// Transaction controller
// Robert Becthold

const express = require('express')
const api = express.Router()
const LOG = require('../utils/logger.js')
const find = require('lodash.find')
const remove = require('lodash.remove')
const Model = require('../models/transaction.js')
const passport = require('../config/passportConfig.js')
const notfoundstring = 'Could not find a transaction matching that id'

// 2 JSON METHODS
// find all
api.get('/findall', passport.isAuthenticated, (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    const data = req.app.locals.transactions.query
    res.send(JSON.stringify(data))
})

// find one by id
api.get('findone/:id', passport.isAuthenticated, (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    const id = parseInt(req.params.id)
    const data = req.app.locals.transactions.query
    const item = find(data, {_id: id})
    if (!item) {return res.end(notfoundstring)}
    res.send(JSON.stringify(item))
})


// 5 VIEW HANDLERS
// default for index.ejs
api.get('/', passport.isAuthenticated, (req, res) => {
    res.render('transaction/index.ejs')
})

// get create
api.get('/create', passport.isAuthenticated, (req, res) => {
    LOG.info(`Handling GET /create${req}`)
    const item = new Model()
    LOG.debug(JSON.stringify(item))
    res.render('transaction/create.ejs',
      {
        title: 'Create new transaction',
        layout: 'layout.ejs',
        transaction: item
      })
  })

// get delete/:id
api.get('/delete/:id', passport.isAuthenticated, (req, res) => {
    LOG.info(`Handling GET /delete/:id ${req}`)
    const id = parseInt(req.params.id, 10) // base 10
    const data = req.app.locals.transactions.query
    const item = find(data, { _id: id })
    if (!item) { return res.end(notfoundstring) }
    LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
    return res.render('transaction/delete.ejs',
      {
        title: 'Delete transaction',
        layout: 'layout.ejs',
        transaction: item
      })
  })

// get edit/:id
api.get('/edit/:id', passport.isAuthenticated, (req, res) => {
    LOG.info(`Handling GET /edit/:id ${req}`)
    const id = parseInt(req.params.id, 10) // base 10
    const data = req.app.locals.transactions.query
    const item = find(data, { _id: id })
    if (!item) { return res.end(notfoundstring) }
    LOG.info(`RETURNING VIEW FOR${JSON.stringify(item)}`)
    return res.render('transaction/edit.ejs',
      {
        title: 'transactions',
        layout: 'layout.ejs',
        transaction: item
      })
  })

// get details/:id
api.get('/details/:id', passport.isAuthenticated, (req, res) => {
    LOG.info(`Handling GET /details/:id ${req}`)
    const id = parseInt(req.params.id, 10) // base 10
    const data = req.app.locals.transactions.query
    const item = find(data, { _id: id })
    if (!item) { return res.end(notfoundstring) }
    LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
    return res.render('transaction/details.ejs',
      {
        title: 'transaction Details',
        layout: 'layout.ejs',
        transaction: item
      })
  })


// 3 CONTROLLER ACTION METHODS
// post new
api.post('/save', passport.isAuthenticated, (req, res) => {
    LOG.info(`Handling POST ${req}`)
    LOG.debug(JSON.stringify(req.body))
    const data = req.app.locals.transactions.query
    const item = new Model()
    LOG.info(`NEW ID ${req.body._id}`)
    item._id = parseInt(req.body._id, 10) // base 10
    item.transactionnum = req.body.transactionnum
    item.transactiontype = req.body.transactiontype
    item.transactionamount = req.body.transactionamount
    item.transactiondate = req.body.transactiondate
    data.push(item)
    LOG.info(`SAVING NEW transaction ${JSON.stringify(item)}`)
    return res.redirect('/transaction')
})

// post update
api.post('/save/:id', passport.isAuthenticated, (req, res) => {
    LOG.info(`Handling SAVE request ${req}`)
    const id = parseInt(req.params.id)
    LOG.info(`Handling SAVING ID=${id}`)
    const data = req.app.locals.transactions.query
    const item = find(data, { _id: id })
    if (!item) { return res.end(notfoundstring) }
    LOG.info(`ORIGINAL VALUES ${JSON.stringify(item)}`)
    LOG.info(`UPDATED VALUES: ${JSON.stringify(req.body)}`)
    item.transactionnum = req.body.transactionnum
    item.transactiontype = req.body.transactiontype
    item.transactionamount = req.body.transactionamount
    item.transactiondate = req.body.transactiondate
    LOG.info(`SAVING UPDATED transaction ${JSON.stringify(item)}`)
    return res.redirect('/transaction')
})

// post delete
api.post('/delete/:id', passport.isAuthenticated, (req, res) => {
    LOG.info(`Handling DELETE request ${req}`)
    const id = parseInt(req.params.id, 10) // base 10
    LOG.info(`Handling REMOVING ID=${id}`)
    const data = req.app.locals.transactions.query
    let item = find(data, { _id: id })
    if (!item) {
        return res.end(notfoundstring)
    }
    item = remove(data, { _id: id })
    LOG.info(`Permanently deleted item ${JSON.stringify(item)}`)
    return res.redirect('/transaction')
})

module.exports = api
