//Brianne
//using puppy.js as reference

const express = require('express')
const api = express.Router()
const Model = require('../models/user.js')
const find = require('lodash.find')
const remove = require('lodash.remove')
const notfoundstring = 'users'
const passport = require('../config/passportConfig.js')
const LOG = require('../utils/logger.js')
LOG.info('Starting controllers/users.js.')

// RESPOND WITH JSON DATA  --------------------------------------------
// based off puppy.js

// GET all JSON
api.get('/findall', passport.isAuthenticated, (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    const data = req.app.locals.users.query
    res.send(JSON.stringify(data))
  })
  
  // GET one JSON by ID
  api.get('/findone/:id', passport.isAuthenticated, (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    const id = parseInt(req.params.id, 10) // base 10
    const data = req.app.locals.users.query
    const item = find(data, { _id: id })
    if (!item) { return res.end(notfoundstring) }
    res.send(JSON.stringify(item))
  })
  
  // RESPOND WITH VIEWS  --------------------------------------------
  
  // GET to this controller base URI (the default)
  api.get('/', passport.isAuthenticated, (req, res) => {
    res.render('user/index.ejs')
  })
  
  // GET create
  api.get('/create', passport.isAuthenticated, (req, res) => {
    LOG.info(`Handling GET /create${req}`)
    const item = new Model()
    LOG.debug(JSON.stringify(item))
    res.render('user/create.ejs',
      {
        title: 'Create user',
        layout: 'layout.ejs',
        user: item
      })
  })
  
  // GET /delete/:id
  api.get('/delete/:id', passport.isAuthenticated, (req, res) => {
    LOG.info(`Handling GET /delete/:id ${req}`)
    const id = parseInt(req.params.id, 10) // base 10
    const data = req.app.locals.users.query
    const item = find(data, { _id: id })
    if (!item) { return res.end(notfoundstring) }
    LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
    return res.render('user/delete.ejs',
      {
        title: 'Delete user',
        layout: 'layout.ejs',
        user: item
      })
  })
  
  // GET /details/:id
  api.get('/details/:id', passport.isAuthenticated, (req, res) => {
    LOG.info(`Handling GET /details/:id ${req}`)
    const id = parseInt(req.params.id, 10) // base 10
    const data = req.app.locals.users.query
    const item = find(data, { _id: id })
    if (!item) { return res.end(notfoundstring) }
    LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
    return res.render('user/details.ejs',
      {
        title: 'user Details',
        layout: 'layout.ejs',
        user: item
      })
  })
  
  // GET one
  api.get('/edit/:id', passport.isAuthenticated, (req, res) => {
    LOG.info(`Handling GET /edit/:id ${req}`)
    const id = parseInt(req.params.id, 10) // base 10
    const data = req.app.locals.users.query
    const item = find(data, { _id: id })
    if (!item) { return res.end(notfoundstring) }
    LOG.info(`RETURNING VIEW FOR${JSON.stringify(item)}`)
    return res.render('user/edit.ejs',
      {
        title: 'users',
        layout: 'layout.ejs',
        user: item
      })
  })
  
  // HANDLE EXECUTE DATA MODIFICATION REQUESTS --------------------------------------------
  
  // POST new
  api.post('/save', passport.isAuthenticated,  (req, res) => {
    LOG.info(`Handling POST ${req}`)
    LOG.debug(JSON.stringify(req.body))
    const data = req.app.locals.users.query
    const item = new Model()
    LOG.info(`NEW ID ${req.body.id}`)
    item._id = parseInt(req.body._id, 10) // base 10
    item.name = req.body.name
    item.age = parseInt(req.body.age, 10)
      data.push(item)
      LOG.info(`SAVING NEW user ${JSON.stringify(item)}`)
      return res.redirect('/user')
    }
  )
  
  // POST update
  api.post('/save/:id', passport.isAuthenticated,  (req, res) => {
    LOG.info(`Handling SAVE request ${req}`)
    const id = parseInt(req.params.id, 10) // base 10
    LOG.info(`Handling SAVING ID=${id}`)
    const data = req.app.locals.users.query
    const item = find(data, { _id: id })
    if (!item) { return res.end(notfoundstring) }
    LOG.info(`ORIGINAL VALUES ${JSON.stringify(item)}`)
    LOG.info(`UPDATED VALUES: ${JSON.stringify(req.body)}`)
    item.name = req.body.name
    item.age = parseInt(req.body.age, 10)
      LOG.info(`SAVING UPDATED user ${JSON.stringify(item)}`)
      return res.redirect('/user')
    }
  )
  
  // DELETE id (uses HTML5 form method POST)
  api.post('/delete/:id', passport.isAuthenticated, (req, res) => {
    LOG.info(`Handling DELETE request ${req}`)
    const id = parseInt(req.params.id, 10) // base 10
    LOG.info(`Handling REMOVING ID=${id}`)
    const data = req.app.locals.users.query
    let item = find(data, { _id: id })
    if (!item) {
      return res.end(notfoundstring)
    }
    item = remove(data, { _id: id })
    LOG.info(`Permanently deleted item ${JSON.stringify(item)}`)
    return res.redirect('/user')
  })

  module.exports = api